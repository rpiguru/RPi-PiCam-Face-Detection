import sys
import threading
import time
import cv2
import os
import numpy
from face_detector import detect_face
from feature_extractor import extract_feature
from settings import RESOLUTION, SHOW_FACE, LED_PIN, MY_FEATURE, THRESHOLD


def is_rpi():
    return 'arm' in os.uname()[4]


if is_rpi():
    from picamera import PiCamera
    from picamera.array import PiRGBArray
    import RPi.GPIO as GPIO

    cam = PiCamera(resolution=RESOLUTION)
    raw_capture = PiRGBArray(cam, cam.resolution)
    time.sleep(2)

    GPIO.setmode(GPIO.BCM)
    GPIO.setup(LED_PIN, GPIO.OUT)


class RPiFaceDetector(threading.Thread):

    def __init__(self):
        super(RPiFaceDetector, self).__init__()

    def run(self):
        if is_rpi():
            run_picamera()
        else:
            cap = cv2.VideoCapture(0)
            while True:
                ret, frame = cap.read()
                if not ret:
                    print('Cannot find camera')
                    sys.exit(1)
                frame = cv2.resize(frame, (800, 600))
                faces = detect_face(frame)
                if faces:
                    print('Face is detected!')
                    feature = extract_feature(faces[0]['frame'])
                    break

            while True:
                ret, frame = cap.read()
                frame = cv2.resize(frame, (800, 600))
                faces = detect_face(frame)
                if faces:
                    for i, r in enumerate([_f['rect'] for _f in faces]):
                        f = extract_feature(faces[i]['frame'])
                        dist = numpy.linalg.norm(f - feature)
                        cv2.rectangle(frame, (r[0], r[1]), (r[0] + r[2], r[1] + r[3]), (0, 0, 255), 2)
                        cv2.putText(frame, str(dist), (r[0], r[1] - 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 2)

                cv2.imshow("Faces", frame)

                key = cv2.waitKey(1) & 0xFF
                # if the `q` key was pressed, break from the loop
                if key == ord("q"):
                    break

            cv2.destroyAllWindows()


def run_picamera():
    for _img in cam.capture_continuous(raw_capture, format='bgr', use_video_port=True):
        # s_time = time.time()
        _frame = numpy.asarray(_img.array)
        _resized_frame = cv2.resize(_frame, RESOLUTION)
        _faces = detect_face(_resized_frame)
        if _faces:
            _face = _faces[0]
            f = extract_feature(_face['frame'])
            dist = numpy.linalg.norm(f - MY_FEATURE)
            print('Face is detected! Rect: {}, similarity: {}'.format(_face['rect'], dist))
            GPIO.output(LED_PIN, GPIO.HIGH if dist < THRESHOLD else GPIO.LOW)

            r = _face['rect']
            cv2.rectangle(_resized_frame, (r[0], r[1]), (r[0] + r[2], r[1] + r[3]), (0, 0, 255), 2)
            cv2.putText(_resized_frame, str(dist), (r[0], r[1] - 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 2)
        else:
            GPIO.output(LED_PIN, GPIO.LOW)

        if SHOW_FACE:
            cv2.imshow("Faces", _resized_frame)

        key = cv2.waitKey(1) & 0xFF
        # if the `q` key was pressed, break from the loop
        if key == ord("q"):
            break

        raw_capture.truncate(0)

    cam.close()
    cv2.destroyAllWindows()


if __name__ == '__main__':

    d = RPiFaceDetector()
    d.start()
