#!/usr/bin/env bash

sudo apt-get update && sudo apt-get autoremove
sudo apt-get install -y build-essential
sudo apt-get install -y cmake git libgtk2.0-dev pkg-config libavcodec-dev libavformat-dev libswscale-dev
sudo apt-get install -y python-dev python-numpy libtbb2 libtbb-dev libjpeg-dev libpng-dev libtiff-dev libjasper-dev libdc1394-22-dev
sudo apt-get install -y libavcodec-dev libavformat-dev libswscale-dev libv4l-dev
sudo apt-get install -y libxvidcore-dev libx264-dev
sudo apt-get install -y libgtk-3-dev
sudo apt-get install -y libatlas-base-dev gfortran
sudo apt-get install -y python2.7-dev python3.5-dev

cd ~
sudo rm -r OpenCV-3.3/
sudo rm -r opencv-3.3.0/
wget https://github.com/opencv/opencv/archive/3.3.0.zip
unzip 3.3.0.zip
rm 3.3.0.zip

sudo pip install -U pip
sudo pip3 install -U pip
sudo pip install -U numpy
sudo pip3 install -U numpy

cd opencv-3.3.0
mkdir -p build
cd build
cmake -D CMAKE_BUILD_TYPE=RELEASE -D CMAKE_INSTALL_PREFIX=/usr/local -D WITH_OPENGL=ON -D FORCE_VTK=ON \
      -D WITH_TBB=ON -D WITH_GDAL=ON -D WITH_XINE=ON -D BUILD_EXAMPLES=OFF -D WITH_V4L=ON -D INSTALL_C_EXAMPLES=OFF ..

echo 'Compiling openCV...'
make
sudo make install
sudo ldconfig

sudo mv /usr/local/lib/python3.5/dist-packages/cv2.cpython-35m-x86_64-linux-gnu.so /usr/local/lib/python3.5/dist-packages/cv2.so

cd ~
sudo rm -r opencv-3.3.0/

echo "Now installed openCV 3.3 successfully. Please check the version of the installed openCV."
