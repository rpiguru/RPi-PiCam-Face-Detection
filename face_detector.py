import os
import cv2


path = os.path.join(os.path.dirname(__file__), 'model', 'haarcascade_frontalface_alt.xml')
cascade = cv2.CascadeClassifier(path)


def detect_face(frame):
    result_data = []
    gray_frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    results = cascade.detectMultiScale(gray_frame, scaleFactor=1.1, minNeighbors=3, minSize=(200, 200),
                                       flags=cv2.CASCADE_SCALE_IMAGE)

    if results != ():
        for r in results:
            # NOTE: its img[y: y + h, x: x + w] and *not* img[x: x + w, y: y + h]
            result_data.append({
                'rect': r,
                'frame': frame[r[1]:r[1] + r[3], r[0]:r[0] + r[2]],
            })

    # Sort frames with their sizes
    sorted_data = sorted(result_data, key=lambda f: f['rect'][2] * f['rect'][3], reverse=True)
    return sorted_data
