import numpy
import dlib
import os
from settings import MY_FEATURE

shape_path = os.path.join(os.path.dirname(__file__), 'model', 'shape_predictor_5_face_landmarks.dat')
shape_predictor = dlib.shape_predictor(shape_path)

recog_path = os.path.join(os.path.dirname(__file__), 'model', 'dlib_face_recognition_resnet_model_v1.dat')
recognizer = dlib.face_recognition_model_v1(recog_path)


def extract_feature(frame):
    h, w = frame.shape[:2]
    face_rect = dlib.rectangle(int(0), int(0), int(w), int(h))

    shape = shape_predictor(frame, face_rect)
    face_description = recognizer.compute_face_descriptor(frame, shape)

    return numpy.array(face_description)


if __name__ == '__main__':

    import glob
    import cv2
    from face_detector import detect_face

    my_feature = numpy.array(MY_FEATURE)

    for img in glob.glob(os.path.expanduser('~/Downloads/*.JPG')):
        _frame = cv2.resize(cv2.imread(img), (800, 600))
        _faces = detect_face(_frame)
        if _faces:
            for f in _faces:
                desc = extract_feature(f['frame'])
                distance = numpy.linalg.norm(desc - my_feature)
                print(img, distance)
        else:
            print(img, 'No Face found')

    cv2.waitKey(0)
    cv2.destroyAllWindows()
