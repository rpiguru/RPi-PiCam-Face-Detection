#!/usr/bin/env bash

cur_dir="$( cd "$(dirname "$0")" ; pwd -P )"

sudo apt-get install -y at-spi2-core

bash ${cur_dir}/install_opencv.sh

sudo pip3 install picamera "picamera[array]"
sudo pip3 install dlib

echo "start_x=1" | sudo tee -a /boot/config.txt

sudo sed -i -- "s/^exit 0/(cd \/home\/pi\/RPi-PiCam-Face-Detection \&\& python3 main.py)\&\\nexit 0/g" /etc/rc.local

sudo reboot
