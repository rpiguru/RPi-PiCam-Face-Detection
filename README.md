# Simple Face Detector using PiCamera and OpenCV

## Installation

- Clone this repository:

        cd ~
        git clone https://gitlab.com/rpiguru/RPi-PiCam-Face-Detection

- Install now.

        cd ~/RPi-PiCam-Face-Detection
        bash install.sh


## How it works

1. Detects the face using **HAAR Cascade** Classifier of OpenCV.
2. Extract the features of the detected face using **dlib**
3. Compare the detected feature with the pre-extracted feature to detect a certain face.
4. Turn LED ON if the detected feature is very similar.
